# README #

*Description*

How to deploy example project

Required System
docker >= 18.xx.xx

docker-compose
```
curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

# Example
  - `docker-compose.yaml` run without previous build image
  - `docker-compose.withci.yaml` need build image first
  - `nginx/conf.d/app.conf` virtualhost
  - `standalone-node` folder app for running cron
  - `node-express-example` folder app for webserver using express module
  - `mysql` folder database config and data is mount outside container
  - `php-custom-fpm` folder config for fpm and source code to run app
  - `.gitlab-ci.yaml` file for gitlab ci/cd
## 1. First We need do something
  - Git pull this repo, and cd into repo
  - File InitDB `./mysql/init.sql` for create schemar
  - App Php: Put all source code php into `php-custom-fpm/source-code/public`
  - Node Standalone: put source code into `standalone-node` and cron file `standalone-node/cron.d/file-cron`
  - Node Express Webserver: put source code into `node-express-example`
## 1.1 Guide deploy with docker-compose.yaml (Choose one option 1.1 or 1.2)
  - Run command `# docker-compose up -d --build` all image is built then container will run
## 1.2 Guide deploy with docker-compose.withci.yaml
  - Git commit source code to gitlab. Gitlab will run gitlab runner for building image(File Config `.gitlab-ci.yml`)
  - After CI gitlab runner successed. We will get 3 images. Change those images to `docker-compose.withci.yaml`
  - Run command `# docker-compose up -f docker-compose.withci.yaml -d` all container is run
## 2. Test
  - For example config. I put 2 domain `app-php.example` and `app-node-express.example` (in file `nginx/conf.d/app.conf`), for working example please put 2 domain to `/etc/hosts`
  ```
  #example /etc/hosts
  104.250.159.xxx app-node-express.example
  104.250.159.xxx app-php.example
  ```
  - App standalone-node will insert row into db
  - Test express app `http://app-node-express.example` will show data in table `persons`
  - Test php app `http://app-php.example/example.php` will show number row table `persons`
## 3. Describe
  - Currently i put all project into one repo, Acctually we should put separate. And each other will have .gitlab-ci.yaml and Dockerfile
  - This example for stateless app.
  - Approach Config.
    + All Config should read from environment variable
    + You could put in .env file and can override when create container (docker-compose file)
  - Service discovery
    + We use service discovery so host connect will be `name of service`, example mysql have name `db-mysql`, and other service connect mysql via domain `db-mysql` instead of ip