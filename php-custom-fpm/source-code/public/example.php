<?php
  $conn = new mysqli("db-mysql", "root", "your_mysql_root_password", "example_database");
  
  if ($conn->connect_error) {
    die("ERROR: Unable to connect: " . $conn->connect_error);
  } 

  echo 'Connected to the database.<br>';

  $result = $conn->query("SELECT * FROM persons");

  echo "Number of rows: $result->num_rows";

  $result->close();

  $conn->close();
?>
