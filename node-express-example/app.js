var express = require('express');
var app = express();
const dotenv = require('dotenv');

dotenv.config();

var mysql = require('mysql');
var connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});



app.get('/', function (req, res) {
    connection.query('select * from persons', function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});