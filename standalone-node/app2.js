var mysql = require('mysql');
const dotenv = require('dotenv');

dotenv.config();

console.log('APP 2 ...');

var conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});

conn.connect(function (err) {
    if (err) throw err;
    console.log("Connected and Exit!");
    process.exit(0);
});