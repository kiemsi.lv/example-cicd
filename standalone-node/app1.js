var mysql = require('mysql');
const dotenv = require('dotenv');

dotenv.config();

console.log('APP 1 ...');

var conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});

conn.connect(function (err) {
    if (err) throw err;
    console.log("Connected And Insert!");

    var sql1 = "INSERT INTO persons VALUES(1,'ANNA','CC','aaa','ddd')";

    conn.query(sql1, function (err, results) {
        if (err) throw err;
        console.log("ADD ONE LINE AND EXIT");
        process.exit(0);
    });
});